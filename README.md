    ___ _  _ ____   ____ ____ ___  _ ____ ___ ____ ___    ___  ____ ____ _  _
     |  |--| |===   |--< |--| |__> | |--|  |  |=== |__>   |==] [__] [__] |-:_


Tools for the creation of the Radiated Book from [Mondothèque wiki](http://www.mondotheque.be/wiki/index.php/Category:publication)


## Dependencies:
* Python libraries: html5lib, mwclient
* [Weasyprint](http://weasyprint.org/docs/) - and its own dependencies
	(Needed for Debian: sudo apt-get install libffi-dev; sudo pip install cairocffi; sudo pip install weasyprint )
* [phantomjs](http://phantomjs.org/download.html) - renders javascript

## Run
```make folders``` makes folder structure 

```make download``` downloads the book's wiki pages as html into text-html/ (NOTE: no processing is performed to the html src files at this stage. Only in ```make book.html``` are the files processed)

```make download_imgs``` downloads the book's `imgs/`


```make book.html``` processes the html files onto 1 single html - creating order, 

```make book.html mode=local``` will use the local images, instead of pulling remote ones - **MUCH FASTER**; **REQUIRES** `make download_imgs`

```make book.pdf ``` produces the book.pdf


## Book structure

* div.bookwrap
   * div.article
   * div.article
   * div.article
   * ...


## Fonts in weasy print
* @font-face is not supported
* but weasyprint can uses system fonts (found by fontconfig `fc-list -f "%-40{family}%{style}\n" `)



## TO DO
### connecting design to Mediawiki structures and devices
* **wrapping transclusions and include metadata of the transclusion - source (is the transclusion noted in html source) - **where are the transclusions**: <http://www.mondotheque.be/wiki/index.php?title=A_bag_but_is_language_nothing_of_words> has some **
    * CONFUSING...
		
### connecting design to Otelt's thinking
* metadata on top of each article - **DONE**
* paragraph numbering as a way to compensate the lack of page numbers in TOC 

* place footnotes on side or **inside** body text: **inside the ref** `<sup>`.
    * Requires Jquery
    * get references: `$('sup.reference')`
	* for each reference get its child a href == the id of corresponding footnote `$(this).children()`?? `.attr('href')`
	* from var href search for footnote element `$(fnhref)` 
	* get text
	* replace the `sup.reference` child `a` with the content from the footnote
	
	
### Sections
* Property::Person - wider tables with more content


### Typography
* **font-size is not responding**



### Rendering Javascript
Weasyprint does not render JS. To tackle that shortcoming we'll use the headless WebKit api  [phantomjs](http://phantomjs.org/download.html).

* Javascript can be included in book.html, like in any other webpage
* (jquey is also included, as can other JS libs)
* book.html renders the JS in the makefile, when `make book.html`
* PDF includes the alterations created by JS

#### Cases to use:
* add articles page numbers to TOC
* add side navigation bar for articles
* **Could PhantomJS be a solution for capturing the maps???**

More on preprocessing JS with PhantomJS in <https://www.smashingmagazine.com/2014/05/love-generating-svg-javascript-move-to-server/>


### Check html - towards the end of document looks very messy


### Navigation bar
As there are no page number on the Index of the book,
on the side of the book pages we want to add a dark bar that indicates the article name.

As the book progress towards its end, the bar lowers its position

For the pages of an article, the bar remains in the same position

It  shall include the article name

#### How to?
* Only on the right pages
* use the article div
* create (with python & less) a rule for the bar under each artcile div. 

`div.article#Property:Person .navigationbar{ margin-top:0; }`

`div.article#Introduction .navigationbar{ margin-top:5%; }`

`...`


`margin-top:` will be have to be devided by the number of articles that are on the index.

## ISSUES
### Weasyprint navigator
trying to run `python -m weasyprint.navigator` to preview the print output.
However I am getting since I installed py lib werkzeuk, which weasyprint uses as server debugger. 

